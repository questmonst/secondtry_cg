#include "SuperHeader.hpp"

void TickFunc(int temp);
GLvoid DrawScene();



GLvoid Keyboard(unsigned char key, int x, int y);
GLvoid SpecialKeyboard(int key, int x, int y);


//GLvoid DrawScene(GLvoid); 곧 버릴것
//GLvoid Reshape(int w, int h);

//이전시간 기록
int PrevTime = 0;

void main(int argc, char** argv) //--- 윈도우 출력하고 콜백함수 설정 
{ 
	//--- 윈도우 생성하기
	glutInit(&argc, argv); // glut 초기화
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA); // 디스플레이 모드 설정
	glutInitWindowPosition(100, 100); // 윈도우의 위치 지정
	glutInitWindowSize(250, 250); // 윈도우의 크기 지정
	glutCreateWindow("Example1"); // 윈도우 생성
	
	//--- GLEW 초기화하기
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) // glew 초기화
	{
		std::cerr << "Unable to initialize GLEW" << std::endl
			;
		exit(EXIT_FAILURE);
	}
	else
		std::cout << "GLEW Initialized\n";


	//콜백함수 지정________________________________________________________________________
	glutDisplayFunc(DrawScene);
	//glutMouseFunc(Mouse); // 키보드 입력 콜백함수 지정
	glutKeyboardFunc(Keyboard); // 키보드 입력 콜백함수 지정
	glutSpecialFunc(SpecialKeyboard);

	//Init이후 지난시간 체크_______________________________________________________________
	PrevTime = glutGet(GLUT_ELAPSED_TIME);
	glutTimerFunc(100, TickFunc, 1);

	//메인 루프 및 삭제___________________________________________________________________
	glutMainLoop();



}
void TickFunc(int temp)
{
	////시간 초기화
	int currTime = glutGet(GLUT_ELAPSED_TIME);
	int elapsedTime = currTime - PrevTime;
	float elapsedTimeInSec = (float)elapsedTime / 1000.f;
	PrevTime = currTime;

	std::cout<<"CurrTime :"<<currTime<<", elapsed Time Sec :"<< elapsedTimeInSec<<std::endl;

	//추후: 모든 객체 업데이트하는 함수
	//추후: 모든 객체 렌더하는 함수

	glutSwapBuffers();

	glutTimerFunc(100, TickFunc, 0);//0.1초에 한번 콜된다는 뜻.
}

GLvoid Keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'p'://육면체로 전환

		break;

	default:
		std::cout << key << std::endl;
		break;
	}
}

GLvoid SpecialKeyboard(int key, int x, int y)
{
	using namespace std;

	if (key == GLUT_KEY_UP)
	{
		cout << "up" << endl;
	}
	if (key == GLUT_KEY_DOWN)
	{
		cout << "down" << endl;
	}
	if (key == GLUT_KEY_LEFT)
	{
		cout << "left" << endl;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		cout << "right" << endl;
	}
}
GLvoid DrawScene()
{
	//@Hyojae : 아무 기능 없이 남겨놓음.

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f); // 바탕색을 ‘blue’ 로 지정
	glClear(GL_COLOR_BUFFER_BIT); // 설정된 색으로 전체를 칠하기

	glutSwapBuffers(); // 화면에 출력하기
}

